import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  Text,
  View,
} from 'react-native';

import SplashScreen from './src/screens/Splash'
import LoginScreen from './src/screens/Login'
import RegisterScreen from './src/screens/Register'
import AkunScreen from './src/screens/Akun'
import BerandaScreen from './src/screens/Beranda'
import ProdukScreen from './src/screens/Produk'
import StatistikScreen from './src/screens/Statistik'
import SupplyScreen from './src/screens/Supply'
import SwipperScreen from './src/screens/Swipper'

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const RootHome = () => {
  return (
    <Tab.Navigator
      initialRouteName="BerandaScreen"
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color }) => {
          let iconName;

          if (route.name === 'Produk') {
            iconName = 'content-copy'
          } else if (route.name === 'Beranda') {
            iconName = 'home'
          } else if (route.name === 'Akun') {
            iconName = 'account-circle'
          } else if (route.name === 'Statistik') {
            iconName = 'stacked-bar-chart'
          } else if (route.name === 'Supply') {
            iconName = 'shopping-basket'
          }

          // You can return any component that you like here!
          return <Icon name={iconName} size={32} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: '#00A5f4',
        inactiveTintColor: 'gray',
      }}
    >
      <Tab.Screen name="Beranda" component={BerandaScreen} />
      <Tab.Screen name="Produk" component={ProdukScreen}/>
      <Tab.Screen name="Supply" component={SupplyScreen}/>
      <Tab.Screen name="Statistik" component={StatistikScreen}/>
      <Tab.Screen name="Akun" component={AkunScreen} />
    </Tab.Navigator>
  )
}

const App = () => {
  return (
    <NavigationContainer>
        <Stack.Navigator headerMode="none">
          <Stack.Screen name="SplashScreen" component={SplashScreen} />
          <Stack.Screen name="SwipperScreen" component={SwipperScreen} />
          <Stack.Screen name="LoginScreen" component={LoginScreen} />
          <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
          <Stack.Screen name="BerandaScreen" component={RootHome} />
        </Stack.Navigator>
    </NavigationContainer>
  )
}



export default App;
