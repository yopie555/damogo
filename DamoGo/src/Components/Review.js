import React from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import ava from '../assets/branda/ava.png'

const Review = () => {

    return (
        <View style={styles.container7}>
            <Image
                source={ava}
                style={styles.ava}
            />
            <View styles={styles.container8}>
                <Text style={styles.text6}>Roronoa Zoro</Text>
                <Text style={styles.text7}>Enak banget humbergernya, gila bikin nagih serius, makasih banyak ya ...</Text>
            </View>
            <Text style={styles.text5}>1 hari yang lalu</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container7: {
        padding: 7,
        flexDirection: 'row',
    },
    container8: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginRight: 10,
    },
    text5: {
        position: 'absolute',
        right: 10,
        top: 10,
        color: '#aaa'
    },
    text6: {
        fontWeight: 'bold',
        fontSize: 15,
        marginLeft: 10,
        marginTop: 5
    },
    text7: {
        color: '#aaa',
        marginLeft: 10,
        marginRight: 15
    },
    ava: {
        height: 60,
        width: 60,
        borderRadius: 50
    },
})
export default Review;
