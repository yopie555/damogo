import React, { useRef, useEffect, useState } from "react";
import {
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
    Image,
    ScrollView,
    ImageBackground,
    Modal
} from 'react-native';
import { ListItem, } from 'react-native-elements'
import RBSheet from "react-native-raw-bottom-sheet";

import Review from '../Components/Review'
import ProdukModal from '../Components/ProdukModal'

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon2 from 'react-native-vector-icons/AntDesign';
import Icon3 from 'react-native-vector-icons/Foundation';

import pic1 from '../assets/branda/saladmoi.png'
import pic2 from '../assets/branda/logosalad.png'
import pic3 from '../assets/branda/produksalad.png'

const Beranda = () => {
    const list = [
        {
            title: 'Setting',
            icon: 'setting'
        },
        {
            title: 'Customer Support',
            icon: 'customerservice'
        },
        {
            title: 'Cara Memakai',
            icon: 'questioncircleo'
        },
        {
            title: 'Kebijakan Publik Kami',
            icon: 'earth'
        },
        {
            title: 'Kebijakan Pengguna',
            icon: 'user'
        },
        {
            title: 'Jadi Pengguna App Kami Yuk',
            icon: 'QQ'
        },
    ]

    const refRBSheet = useRef();

    const [produkVisible, setProdukVisible] = useState(false);
    return (
        <ScrollView>
            <View style={styles.container}>
                <View>
                    <ImageBackground
                        source={pic1}
                        style={styles.background}
                        blurRadius={1.5}
                    >
                        <View style={styles.container2}>
                            <Image
                                source={pic2}
                                style={styles.image}
                            />
                            <Text style={styles.text}>Salad Moi</Text>
                        </View>
                        <View style={styles.container3}>
                            <Icon
                                name='account'
                                size={25}
                                color={"#000"}
                            />
                            <Text>100 pengikut</Text>
                            <Icon2
                                name='instagram'
                                size={25}
                                color={"#000"}
                            />
                            <Text>@saladmoi</Text>
                            <Icon3
                                name='telephone'
                                size={25}
                                color={"#000"}
                            />
                            <Text>082260426032</Text>
                        </View>
                        <View style={styles.container4}>
                            <Text>Gg. Mbakalan No 14, Jl. Kaliurang No.km 7.6</Text>
                        </View>
                    </ImageBackground>
                </View>
                <View style={styles.container5}>
                    <Text style={styles.text1}>Rating Toko
                        <Icon2
                            name='star'
                            color={"#FFBB00"}
                        />
                        5.0
                    </Text>
                    <Text
                        style={styles.text2}
                        onPress={() => refRBSheet.current.open()}
                    >Lihat Ulasan
                    </Text>
                    <RBSheet
                        ref={refRBSheet}
                        closeOnDragDown={true}
                        closeOnPressMask={false}
                        customStyles={{
                            wrapper: {
                                backgroundColor: "transparent"
                            },
                            draggableIcon: {
                                backgroundColor: "#000"
                            }
                        }}
                    >
                        <ScrollView>
                            <Review />
                            <Review />
                            <Review />
                            <Review />
                            <Review />
                        </ScrollView>
                    </RBSheet>
                </View>
                <View style={styles.container6}>
                    <TouchableOpacity
                        style={styles.btn1}
                        onPress={() => { setProdukVisible(true) }}
                    >
                        <Icon2
                            name='like1'
                            color={"#FFBB00"}
                            size={20}
                            style={styles.icon}
                        />
                        <Text style={styles.text3}>
                            3 Produk Terlaris
                        </Text>
                    </TouchableOpacity>
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={produkVisible}
                        onRequestClose={() => {
                            setProdukVisible(false);
                        }}>
                        <View style={styles.modalContainer}>
                            <ProdukModal setProdukVisible={setProdukVisible} />
                        </View>
                    </Modal>
                    <TouchableOpacity style={styles.btn2}>
                        <Icon
                            name='alarm'
                            color={"#009673"}
                            size={20}
                        />
                        <Text style={styles.text4}>
                            Jam Paling Recomended
                        </Text>
                    </TouchableOpacity>
                </View>
                <View>
                    {
                        list.map((item, i) => (
                            <ListItem key={i} bottomDivider>
                                <Icon2 name={item.icon} size={20} />
                                <ListItem.Content>
                                    <ListItem.Title>{item.title}</ListItem.Title>
                                </ListItem.Content>
                                <ListItem.Chevron />
                            </ListItem>
                        ))
                    }
                </View>
                <Text>Beranda</Text>
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    container2: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 35
    },
    container3: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginHorizontal: 20
    },
    container4: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    container5: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginHorizontal: 15,
        marginTop: 5
    },
    container6: {
        flexDirection: 'row',
        marginHorizontal: 10
    },

    background: {
        width: '100%',
        height: null,
        aspectRatio: 1.78,
    },
    image: {
        height: 75,
        width: 75,
        borderRadius: 50
    },
    text: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 24,
        marginBottom: 20
    },
    text1: {
        fontSize: 15,
        fontWeight: '500',
    },
    text2: {
        color: '#FFBB00',
        fontSize: 15,
        fontWeight: '500',
    },
    text3: {
        color: '#FFBB00',
        fontSize: 15,
        fontWeight: '500',
        marginBottom: 5
    },
    text4: {
        color: '#009673',
        fontSize: 15,
        fontWeight: '500',
        marginBottom: 5
    },

    btn1: {
        borderWidth: 1,
        width: '47.5%',
        marginHorizontal: 5,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 5,
        borderRadius: 3,
        backgroundColor: '#FFF2DE',
        marginTop: 10,
        borderColor: '#FFF2DE'
    },
    btn2: {
        borderWidth: 1,
        width: '47.5%',
        marginHorizontal: 5,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 5,
        borderRadius: 3,
        backgroundColor: '#DAFFD9',
        marginTop: 10,
        borderColor: '#DAFFD9'
    },
    icon: {
        marginVertical: 5
    },
    modalContainer: {
        alignItems: 'center',
        top: '7%',
    },

})
export default Beranda;
