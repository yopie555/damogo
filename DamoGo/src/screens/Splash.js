import React, { useEffect } from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';

import Logo from '../assets/DamoGO.png'

function SplashScreen({ navigation }) {
    setTimeout(() => {
        navigation.replace('SwipperScreen');
    }, 3000);
    return (
        <View
            style={{
                backgroundColor: '#FAFAFF',
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
            }}>
            <Image style={styles.image} source={Logo} />
            <Text style={{ color: '#00A5f4', fontSize: 20, fontFamily: 'Roboto' }}>Create Taste, Not Waste</Text>
        </View>
    );
}


const styles = StyleSheet.create({
    image: {
        width: 150,
        height: 150,
        marginBottom: 15,
    },
})

export default SplashScreen;