import React, { useState } from 'react';
import {
    TextInput,
    StyleSheet,
    TouchableOpacity,
    View,
    Text,
    StatusBar,
    Image,
} from 'react-native';

import Logo from '../assets/DamoGO.png'

const Login = ({ navigation }) => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");


    return (
        <View style={style.container}>
            <Image
                style={style.logo}
                source={Logo}
            />
            <Text style={style.tag}>Login</Text>
            <TextInput
                style={style.inputText}
                onChangeText={(email) => setEmail(email)}
                value={email}
                placeholder="emali"
            />
            <TextInput
                style={style.inputText}
                secureTextEntry={true}
                onChangeText={(password) => setPassword(password)}
                value={password}
                placeholder="Password"
            />
            <TouchableOpacity
                style={style.loginBtn}
                onPress={() => navigation.navigate("BerandaScreen")}>
                <Text style={style.textLogin}>Masuk</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={style.loginBtn1}
                onPress={() => navigation.navigate("RegisterScreen")}>
                <Text style={style.textLogin1}>Daftar Sebagai Mitra</Text>
            </TouchableOpacity>
            <Text style={style.textAccount}>
                Dengan masuk, kamu menyetujui Kebijakan Penggunaan dan Kebijakan Publik kami
            </Text>
        </View>
    );
};

const style = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    logo: {
        width: '45%',
        height: null,
        aspectRatio: 1,
        marginBottom: 30,
    },
    tag: {
        color: '#00A5f4',
        fontWeight: 'bold',
        fontSize: 18,
        width: '90%',
        padding: 5
    },
    inputText: {
        width: '90%',
        borderRadius: 7,
        padding: 10,
        color: 'white',
        marginBottom: 10,
        borderColor: '#00A5f4',
        borderBottomWidth: 1,
    },
    loginBtn: {
        width: '90%',
        borderRadius: 7,
        alignItems: 'center',
        padding: 10,
        marginTop: 20,
        borderWidth: 1,
        backgroundColor: '#00A5f4'
    },
    loginBtn1: {
        width: '90%',
        borderRadius: 7,
        alignItems: 'center',
        padding: 10,
        marginTop: 20,
        borderWidth: 1,
        borderColor: '#00A5f4',
        backgroundColor: '#FFFFFF'
    },
    textAccount: {
        color: '#a0a0a0',
        marginTop: 15,
        marginBottom: 10,
        textAlign: 'center',
        marginHorizontal: 20
    },
    textLogin: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 16,
    },
    textLogin1: {
        color: '#00A5f4',
        fontWeight: 'bold',
        fontSize: 16,
    },
});

export default Login;
