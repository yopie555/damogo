import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native'

import Swiper from 'react-native-swiper'

import Logo1 from '../assets/1.png'
import Logo2 from '../assets/2.png'
import Logo3 from '../assets/3.png'
import Logo4 from '../assets/4.png'


const Swipper = ({navigation}) => {
    return (
        <Swiper style={styles.wrapper} >
            <View style={styles.slide1}>
                <Image style={styles.image} source={Logo1} />
                <Text style={styles.text}>Tingkatkan Penjualan Kamu</Text>
                <Text style={styles.text1}>
                    Yuk jual kembali makanan kamu yang belum laku dan hampir terbuang. Sekaligus selamatkan lingkungan sekitar kamu dari food waste!
                </Text>
                <View style={styles.text2}>
                    <Text 
                    style={styles.text3}
                    onPress={() => navigation.navigate("LoginScreen")}
                    >Lewati</Text>
                </View>
            </View>
            <View style={styles.slide1}>
                <Image style={styles.image} source={Logo2} />
                <Text style={styles.text}>Dapetin Customer</Text>
                <Text style={styles.text1}>
                    Tingkatkan exposure gerai kamu dan branding perusahaan dengan aplikasi kami
                </Text>
            </View>
            <View style={styles.slide1}>
                <Image style={styles.image} source={Logo3} />
                <Text style={styles.text}>Platform Marketing Gratis!</Text>
                <Text style={styles.text1}>
                    Perkuat branding gerai kamu dengan menunjukkan ke masyarakat luas bahwa kamu peduli terhadap problema food waste, sekaligus bertanggung jawab sebagai owner dari usaha yang kamu manage.
                </Text>
            </View>
            <View style={styles.slide1}>
                <Image style={styles.image} source={Logo4} />
                <Text style={styles.text}>Tanpa Risk</Text>
                <Text style={styles.text1}>
                    Semua gratis. Gratis biaya pendaftaran, ga ada biaya bulanan, ga ada syarat minimum, ga ada biaya yang tersembunyi. Kurang apa coba?
                </Text>
                <View style={styles.text2}>
                    <Text 
                    style={styles.text3}
                    onPress={() => navigation.navigate("LoginScreen")}
                    >Selesai</Text>
                </View>
            </View>
        </Swiper>
    );
}

const styles = StyleSheet.create({
    // wrapper: {},
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        margin: 10
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        //   backgroundColor: '#97CAE5'
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        //   backgroundColor: '#92BBD9'
    },
    text: {
        color: '#00A3E0',
        fontSize: 36,
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 5
    },
    text1: {
        color: '#A0A0A0',
        fontSize: 18,
        // fontWeight: 'bold',
        textAlign: 'center'
    },
    text2: {
        position: 'absolute',
        bottom: 15,
        right: 20
    },
    text3:{
        color: '#00A3E0', 
        fontSize: 18
    },
    image: {
        width: 325,
        height: 325,
        marginBottom: 15,
    },
})

export default Swipper;
